<!DOCTYPE html>
<html>
   <head>
     
      
      <title>Bill Reciept</title>
      <h1>Government og Kerala</h1>
      <style>
        .result{
         color:red;
        }
        td
        {
          text-align:center;
        }
      </style>
   </head>
   <body>
      <section class="mt-3">
         <div class="container-fluid">
         <h4 class="text-center" style="color:green">  </h4>
         {{-- <h6 class="text-center"> <h6> --}}
         <div class="row">
            <div class="col-md-5  mt-4 ">
               <table class="table" style="background-color:#e0e0e0;" >
                 
                  <thead>
                     <tr>
                        <th>Energy charge</th>
                        <th>Duty</th>
                        <th>Fixed charge</th>
                        <th>Meter rent State GST</th>
                        <th>Meter rent cENTAL GST</th>
                        <th>Total Amount</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td scope="row">1</td>
                        <td style="width:60%">
                           <select name="vegitable" id="vegitable"  class="form-control">
                             @foreach($bill as $row )
                              <option id={{$row->id}} value={{$row->customer_no}}>
                                {{$row->customer_no}}
                              </option>
                             @endforeach
                           </select>
                        </td>
                        <td style="width:1%">
                          <input type="number" id="qty" min="0" value="0" class="form-control">
                        </td>
                        <td>
                           <h5 class="mt-1" id="price" ></h5>
                        </td>
                        <td><button id="add" class="btn btn-success">calculate</button></td>
                     </tr>
                     <tr>
                     </tr>    
                  
                  </tbody>
               </table>
               <div role="alert" id="errorMsg" class="mt-5" >
                 <!-- Error msg  -->
              </div>
            </div>
            <div class="col-md-7  mt-4" style="background-color:#f5f5f5;">
               <div class="p-4">
                  <div class="text-center">
                     <h4>Receipt</h4>
                  </div>
                  <span class="mt-4"> Time : </span><span  class="mt-4" id="time"></span>
                  <div class="row">
                     <div class="col-xs-6 col-sm-6 col-md-6 ">
                        <span id="day"></span> : <span id="year"></span>
                     </div>
                     <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                        <p>consumer_no</p>
                     </div>
                  </div>
                  <div class="row">
                     </span>
                     <table id="receipt_bill" class="table">
                        <thead>
                           <tr>
                           <th>Energy charge</th>
                        <th>Duty</th>
                        <th>Fixed charge</th>
                        <th>Meter rent State GST</th>
                        <th>Meter rent cENTAL GST</th>
                             
                              <th class="text-center">Total Amount</th>
                           </tr>
                        </thead>
                        <tbody id="new" >
                          
                        </tbody>
                        <tr>
                           <td> </td>
                           <td> </td>
                           <td> </td>
                           <td class="text-right text-dark" >
                                <h5><strong>Sub Total:  ₹ </strong></h5>
                                <p><strong>Tax (5%) : ₹ </strong></p>
                           </td>
                           <td class="text-center text-dark" >
                              <h5> <strong><span id="subTotal"></strong></h5>
                              <h5> <strong><span id="taxAmount"></strong></h5>
                           </td>
                        </tr>
                        <tr>
                           <td> </td>
                           <td> </td>
                           <td> </td>
                           <td class="text-right text-dark">
                              <h5><strong>Gross Total: ₹ </strong></h5>
                           </td>
                           <td class="text-center text-danger">
                              <h5 id="totalPayment"><strong> </strong></h5>
                               
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>
<script>
    $(document).ready(function(){
      $('#vegitable').change(function() {
       var ids =   $(this).find(':selected')[0].id;
        $.ajax({
           type:'GET',
           url:'getPrice/{id}',
           data:{id:ids},
           dataType:'json',
           success:function(data)
             {
                
                 $.each(data, function(key, resp)
                 {     
                  $('#price').text(resp.product_price);
                });
             }
        });
      });
       // Code for Sub Total of Vegitables 
       var total = 0;
             $('tbody tr td:last-child').each(function() {
                 var value = parseInt($('#total', this).val());
                 if (!isNaN(value)) {
                     total += value;
                 }
             });
              $('#subTotal').text(total);
                
             // Code for calculate tax of Subtoal 5% Tax Applied
               var Tax = (total * 5) / 100;
               $('#taxAmount').text(Tax.toFixed(2));
  
              // Code for Total Payment Amount
  
              var Subtotal = $('#subTotal').text();
              var taxAmount = $('#taxAmount').text();
  
              var totalPayment = parseFloat(Subtotal) + parseFloat(taxAmount);
              $('#totalPayment').text(totalPayment.toFixed(2)); // Showing using ID 
         
          });
          count++;
         } 
        });