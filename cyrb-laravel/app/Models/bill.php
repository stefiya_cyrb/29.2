<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bill extends Model
{
    use HasFactory;
    public $table='bills';
    protected $fillable=[
   'energy_charge','duty','fixed_charge','meter_rent','meterrent_stateGST',
    'meterrent_centralGST','total_amount',];
    public $timestamps=false;
}
